import { Component, OnInit } from '@angular/core';
import { ProductsService } from '../products.service';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css']
})
export class CartComponent implements OnInit {

  cart = [];
  cartTotalPrice = 0;

  constructor(private productService: ProductsService ) { }

  ngOnInit() {
    this.cart = this.productService.getCart();
    this.cartTotalPrice = this.cart.map(product => Number(product.price) ).reduce((prev, curr) => prev + curr);
  }

  removeFromCart(product) {
    this.productService.removeFromCart(product._id);
  }

}
