import { Component, OnInit } from '@angular/core';
import { ProductsService } from '../products.service';

@Component({
  selector: 'app-productlist',
  templateUrl: './productlist.component.html',
  styleUrls: ['./productlist.component.css']
})
export class ProductlistComponent implements OnInit {

  products = [];

  constructor(private productService: ProductsService) { }

  ngOnInit() {
    this.productService.fetchProducts().subscribe(response => {
      this.productService.products = response;
      this.products = response;
    });
  }

  addProductToCart(product) {
    this.productService.addProductToCart(product._id);
  }

  findProductInCart(product) {
    return this.productService.findProductInCart(product._id).length > 0
  }
}
