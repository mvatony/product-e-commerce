var express = require('express');
var bodyParser = require('body-parser');
var cors = require('cors');
var mongoClient = require('mongodb').MongoClient;

var app = express();
app.use(cors());
app.use(bodyParser());

app.get('/api/products', function(req, resp){
    mongoClient.connect('mongodb://localhost:27017/learnIt', function(err, db){
        if (!err) {
            db.collection('products').find().toArray(function(er, products){
                resp.send(JSON.stringify(products));
            });
        }
    });
});

app.post('/api/checkout', function(req, resp){
    mongoClient.connect('mongodb://localhost:27017/learnIt', function(err, db){
        if (!err) {
            var order = req.body; 
            db.collection('orders').insertOne(order, function(err, reusult){
                if (!err) {
                    resp.send({"message": "Orders placed successfully"});
                } else  {
                    resp.send({"message": "Orders failure"});
                }
            })
        }
    })
});

app.get('/api/orders', function(req, resp){
    mongoClient.connect('mongodb://localhost:27017/learnIt', function(err, db){
        if (!err) {
            db.collection('orders').find().toArray(function(err, orders){
                resp.send(JSON.stringify(orders));
            });
        }
    });
});

app.listen(9000, () => console.log('API started listening...'));