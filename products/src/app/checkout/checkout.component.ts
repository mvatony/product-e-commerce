import { Component, OnInit } from '@angular/core';
import { ProductsService } from '../products.service';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router'

@Component({
  selector: 'app-checkout',
  templateUrl: './checkout.component.html',
  styleUrls: ['./checkout.component.css']
})
export class CheckoutComponent implements OnInit {

cart = [];
cartTotalPrice = 0;

checkoutForm = this.FormBuilder.group({
  firstName: ['', Validators.required],
  lastName: ['', Validators.required],
  email: ['', Validators.required],
  addressOne: ['', Validators.required],
  addressTwo: ['', Validators.required],
  country: ['', Validators.required],
  state: ['', Validators.required],
  zip: ['', Validators.required]
});

  constructor(private router: Router, private FormBuilder: FormBuilder, private productService: ProductsService) { }

  ngOnInit() {
    this.cart = this.productService.getCart();
    this.cartTotalPrice = this.cart.map(product => Number(product.price) ).reduce((prev, curr) => prev + curr, 0);
  }

  checkoutOrder() {
    var order = {
      ...this.checkoutForm.value,
      items: this.cart
    };
    this.productService.checkout(order).subscribe(res => {
      console.log('Order Placed Succesfully');
      this.productService.clearCart();
      this.router.navigate(['/products']);
    })
  }
  
}
