import { Component, OnInit } from '@angular/core';
import { ProductsService } from '../products.service';

@Component({
  selector: 'app-order',
  templateUrl: './order.component.html',
  styleUrls: ['./order.component.css']
})
export class OrderComponent implements OnInit {

  orders = [];

  constructor(private productService: ProductsService) { }

  ngOnInit() {
    this.productService.fetchOrders().subscribe(res =>{
      this.orders = res;
    })
  }

  totalAmount(orders) {
    return orders.reduce((prevOrder, currOrder) => prevOrder = this.totalAmountForEachOrder(currOrder.items), 0)
  }

  totalItemsFromAllOrders(orders) {
    return orders.reduce((prevOrder, currOrder) => prevOrder = currOrder.items.length, 0)
  }

  totalAmountForEachOrder(itemsForEachOrder) {
    return itemsForEachOrder.reduce((previous, current) => previous = Number(current.price), 0)
  }
}
